<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo ucfirst($_SESSION['username']);?></div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span><?php echo ucfirst($_SESSION['authorization']);?></div>
			</div>
			<div class="clear"></div>
		</div>
		<?php
		if($authorization=="Bendahara RW"){
		?>
		<div class="divider"></div>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-database">&nbsp;</em> Master <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="index.php?page=viewuser">
						<span class="fa fa-hotel">&nbsp;</span> User
					</a></li>
					<li><a class="" href="index.php?page=viewwarga">
						<span class="fa fa-group">&nbsp;</span> Warga
					</a></li>
					<li><a class="" href="index.php?page=viewiuran">
						<span class="fa fa-user">&nbsp;</span> Iuran
					</a></li>
					<li><a class="" href="index.php?page=viewaset">
						<span class="fa fa-sort-amount-asc">&nbsp;</span> Aset
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> Laporan <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="index.php?page=viewlaporanwarga">
						<span class="fa fa-pie-chart">&nbsp;</span> Lap.warga
					</a></li>
					<li><a class="" href="index.php?page=viewlaporanaset">
						<span class="fa fa-line-chart">&nbsp;</span> Lap.Aset
					</a></li>
					<li><a class="" href="index.php?page=viewlaporantransaksi">
						<span class="fa fa-line-chart">&nbsp;</span> Lap.pembayaran iuran
					<li><a class="" href="index.php?page=viewpem">
						<span class="fa fa-line-chart">&nbsp;</span> Lap.peminjaman
					</a></li>
					<li><a class="" href="index.php?page=viewpem2">
						<span class="fa fa-line-chart">&nbsp;</span> Lap.Pengembalian
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-4">
				<em class="fa fa-navicon">&nbsp;</em> Pengaturan <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-4">
					<li><a class="" href="index.php?page=ganti">
						<span class="fa fa-key">&nbsp;</span> Ganti Password
					</a></li>
				</ul>
			</li>
			<li><a href="login.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
		<?php
		}elseif($authorization=="Ketua RW"){
		?>
		<div class="divider"></div>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> Laporan <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="index.php?page=viewlaporanwarga">
						<span class="fa fa-arrow-right">&nbsp;</span> Lap.Warga
					</a></li>
					<li><a class="" href="index.php?page=viewlaporanaset">
						<span class="fa fa-arrow-right">&nbsp;</span> Lap.Aset
					</a></li>
					<li><a class="" href="index.php?page=viewlaporantransaksi">
						<span class="fa fa-arrow-right">&nbsp;</span> Lap.Pembayaran Iuran
					</a></li>
					<li><a class="" href="index.php?page=viewpem">
						<span class="fa fa-arrow-right">&nbsp;</span> Lap.Peminjaman
					</a></li>
					<li><a class="" href="index.php?page=viewpem2">
						<span class="fa fa-arrow-right">&nbsp;</span> Lap.Pengembalian
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-4">
				<em class="fa fa-navicon">&nbsp;</em> Pengaturan <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-4">
					<li><a class="" href="index.php?page=ganti">
						<span class="fa fa-key">&nbsp;</span> Ganti Password
					</a></li>
				</ul>
			</li>
			<li><a href="login.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
			</ul>
		<?php
		}elseif($authorization=="Petugas"){
		?>
		<div class="divider"></div>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> Transaksi <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="index.php?page=viewtransaksi">
						<span class="fa fa-arrow-right">&nbsp;</span> Pembayaran Iuran
					</a></li>
				</ul>
			</li>
			<li><a href="login.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
			</ul>
			<?php
		}elseif($authorization=="Sekretaris RW"){
		?>
		<div class="divider"></div>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> Transaksi <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="index.php?page=viewpeminjaman">
						<span class="fa fa-arrow-right">&nbsp;</span> Peminjaman Aset
					</a></li>
					<li><a class="" href="index.php?page=viewpengembalian">
						<span class="fa fa-arrow-right">&nbsp;</span> Pengembalian Aset
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> Laporan <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="index.php?page=viewpem">
						<span class="fa fa-pie-chart">&nbsp;</span> Lap.Peminjaman
					</a></li>
					<li><a class="" href="index.php?page=viewpem2">
						<span class="fa fa-pie-chart">&nbsp;</span> Lap.Pengembalian
					</a></li>
					</ul>
					</li>
					<li class="parent "><a data-toggle="collapse" href="#sub-item-4">
				<em class="fa fa-navicon">&nbsp;</em> Pengaturan <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-4">
					<li><a class="" href="index.php?page=ganti">
						<span class="fa fa-key">&nbsp;</span> Ganti Password
					</a></li>
				</ul>
			</li>
			<li><a href="login.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
			</ul>
	<?php
		}elseif($authorization=="Warga"){
		?>
		<div class="divider"></div>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> Laporan <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="index.php?page=viewlaporantransaksi">
						<span class="fa fa-arrow-right">&nbsp;</span> Lap.Pembayaran Iuran
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-4">
				<em class="fa fa-navicon">&nbsp;</em> Pengaturan <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-4">
					<li><a class="" href="index.php?page=ganti">
						<span class="fa fa-key">&nbsp;</span> Ganti Password
					</a></li>
				</ul>
			</li>
			<li><a href="login.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
			</ul>
	<?php
		}
	?>
	</div><!--/.sidebar-->