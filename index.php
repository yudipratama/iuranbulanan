<?php
session_start();
if(empty($_SESSION['username'])){
	header("location:login.php");
}else{
	if(isset($_SESSION['username'])){
		$user = trim($_SESSION['username']);
	}
	if(isset($_SESSION['authorization'])){
		$authorization = trim($_SESSION['authorization']);
	}
require_once('include/config.php');
$base_url = ('http://'.$_SERVER['HTTP_HOST'].'/Planning/index.php');
	isset ($_GET['page']) ? $page = $_GET['page'] : $page = 'home';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Menu Utama</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	<link href="css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="css/font.css" rel="stylesheet">
	
	<script type="text/javascript" src="js/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</head>
<body>
	<?php
	include 'include/header.php';
	include 'include/sidebar.php';
		?>
	<?php
	//include 'page/home.php';
	//(isset($_SESSION['pesan'])){echo $_SESSION['pesan']; unset($_SESSION['pesan']);}
	if(file_exists('page/'.$page.'.php')){
		include('page/'.$page.'.php');
	}else{
	echo '<div class="well">Error : Aplikasi tidak menemukan adanya file <b>'.$page.'.php </b> pada direktori <b>app/..</b></div>'; 
	}
	?>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<link rel="stylesheet" href="css/jquery-ui.css">
	<script src="js/jquery-ui.js"></script>
	
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script type="text/javascript" src="js/validation.js"></script>

	 <script type="text/javascript">
	 	 // Autocomplete
        $(function() {
            $( ".txtnokk" ).autocomplete({
            source: 'controler/act-autocomplete-warga.php'
            });
        });
		 // Autocomplete
        $(function() {
            $( ".txtidiuran" ).autocomplete({
            source: 'controler/act-autocomplete-iuran.php'
            });
        });
		 // Autocomplete
        $(function() {
            $( ".txtidaset" ).autocomplete({
            source: 'controler/act-autocomplete-aset.php'
            });
        });
	
		function isi_otomatis(){
			var txtnokk = $("#txtnokk").val();
			$.ajax({
				url: 'controler/textwarga.php',
				data:"txtnokk="+txtnokk ,
			}).success(function (data) {
				var json = data,
				obj = JSON.parse(json);
				$('#txtnamawarga').val(obj.nama_warga);
				$('#txtblok').val(obj.blok_rumah);
				$('#txtstatus').val(obj.status_pam);
				$('#txtrt').val(obj.rt);
			});
		}

		//autoview
		function isi_otomatis1(){
			var txtidiuran = $("#txtidiuran").val();
			$.ajax({
				url: 'controler/textiuran.php',
				data:"txtidiuran="+txtidiuran ,
			}).success(function (data) {
				var json = data,
				obj = JSON.parse(json);
				$('#txtnmiuran').val(obj.nama_iuran);
				$('#txtnominal').val(obj.nominal_harga);
			});
		}
		function isi_otomatis2(){
			var txtpembayaran = $("#txtpembayaran").val();
			$.ajax({
				url: 'controler/textperhitungan.php',
				data:"txtpembayaran="+txtpembayaran
			}).success(function (data) {
				var json = data,
				obj = JSON.parse(json);
				$('#txttotal').val(obj.total_harga);
			});
		}
		function isi_otomatis3(){
			var txtidaset = $("#txtidaset").val();
			$.ajax({
				url: 'controler/textaset.php',
				data:"txtidaset="+txtidaset ,
			}).success(function (data) {
				var json = data,
				obj = JSON.parse(json);
				$('#txtnmaset').val(obj.nama_aset);
			});
		}
		function isi_otomatis4(){
			var txtidpeminjaman = $("#txtidpeminjaman").val();
			$.ajax({
				url: 'controler/textpeminjaman.php',
				data:"txtidpeminjaman="+txtidpeminjaman ,
			}).success(function (data) {
				var json = data,
				obj = JSON.parse(json);
				$('#txtnokk').val(obj.no_kk);
				$('#txtstpeminjaman').val(obj.status_peminjaman);
				$('#txtjmlpem').val(obj.jumlah_peminjaman);
			});
		}
	</script>
</body>
</html>
<?php
}
?>