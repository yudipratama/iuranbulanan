<?php
include ('../include/config.php');
session_start();
$printby = $_SESSION['username'];
$date = DATE('d-m-Y');
// $tg1 = $_POST['tg1'];
// $th1 = $_POST['th1'];
 $tgl_mulai = $_POST['tgl_mulai'];
 $tgl_akhir = $_POST['tgl_akhir'];
$nama = $_SESSION['namalengkap'];
if($tgl_akhir < $tgl_mulai){
echo "<script>alert('Tanggal akhir tidak boleh sebelum tanggal awal');window.location='../index.php?page=viewlaporanaset'</script>";
	}
// $w = $th1."-".$tg1;


require('fpdf.php');

$pdf = new FPDF('l','mm','A5');

$pdf->AddPage();

$pdf->SetFont('Arial','B',14);
$pdf->image('logos.png',10,5,20,20);
$pdf->Cell(190,7,'LAPORAN DATA ASET',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,7,'PERUMAHAN GRAHA SEGOVIA RW 006',0,1,'C');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(190,4,'DESA CIAKAR KEC.PANONGAN KAB.TANGERANG-BANTEN',0,1,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(190,7,'PRINT BY '.$printby.'',0,1,'L');
$pdf->Cell(190,5,'PRINT DATE '.$date.'',0,2,'L');
 

$pdf->Cell(10,2,'',0,1);
 
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'ID Aset',1,0);
$pdf->Cell(35,6,'Tanggal Beli',1,0);
$pdf->Cell(30,6,'Nama Aset',1,0);
$pdf->Cell(20,6,'Jumlah',1,0);
$pdf->Cell(35,6,'Harga Satuan',1,0);
$pdf->Cell(35,6,'Total Harga',1,1);

$pdf->SetFont('Arial','',10);

$d = $con->query("SELECT id_aset, tanggal_beli, nama_aset, jumlah, harga_satuan, total_harga_aset FROM tb_aset WHERE date(tanggal_beli) between DATE('$tgl_mulai') AND DATE('$tgl_akhir') ORDER BY tanggal_beli ASC");
    foreach ($d as $dd) {
    $pdf->Cell(20,6,$dd['id_aset'],1,0);
	$pdf->Cell(35,6,$dd['tanggal_beli'],1,0);
	$pdf->Cell(30,6,$dd['nama_aset'],1,0);
    $pdf->Cell(20,6,$dd['jumlah'],1,0);
    $pdf->Cell(35,6,$dd['harga_satuan'],1,0);
	$pdf->Cell(35,6,$dd['total_harga_aset'],1,1);
}
$pdf->SetFont('Arial','',12);
$pdf->Cell(306,18,'Tangerang, '.$date.'',0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->Cell(306,20,''.$nama.'',0,1,'C');
$pdf->Output();
?>