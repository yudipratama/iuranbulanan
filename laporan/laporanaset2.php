<?php
include ('../include/config.php');
session_start();
$printby = $_SESSION['username'];
$date = DATE('d-m-Y');
$th1 = $_POST['th1'];
$nama = $_SESSION['namalengkap'];

require('fpdf.php');

$pdf = new FPDF('l','mm','A5');

$pdf->AddPage();

$pdf->SetFont('Arial','B',16);
$pdf->image('logos.png',10,5,20,20);
$pdf->Cell(190,7,'Data Aset',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,7,'REPORT ACCORDING ASET',0,1,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(190,7,'PRINT BY '.$printby.'',0,1,'L');
$pdf->Cell(190,7,'PRINT DATE '.$date.'',0,2,'L');
 

$pdf->Cell(10,7,'',0,1);
 
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'ID Aset',1,0);
$pdf->Cell(30,6,'Nama Aset',1,0);
$pdf->Cell(20,6,'Jumlah',1,0);
$pdf->Cell(35,6,'Harga Satuan',1,1);

$pdf->SetFont('Arial','',10);

$d = $con->query("SELECT id_aset, nama_aset, jumlah, harga_satuan FROM tb_aset");
    foreach ($d as $dd) {
    $pdf->Cell(20,6,$dd['id_aset'],1,0);
	$pdf->Cell(30,6,$dd['nama_aset'],1,0);
    $pdf->Cell(20,6,$dd['jumlah'],1,0);
    $pdf->Cell(35,6,$dd['harga_satuan'],1,1);
}
 $pdf->SetFont('Arial','',12);
$pdf->Cell(230,30,'Tangerang, '.$date.'',0,1,'C');
$pdf->Cell(230,30,''.$nama.'',0,1,'C');
$pdf->Output();
?>