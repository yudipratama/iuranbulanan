<?php
include ('../include/config.php');
session_start();
$printby = $_SESSION['username'];
$date = DATE('d-m-Y');
// $tg1 = $_POST['tg1'];
// $th1 = $_POST['th1'];
 $tgl_mulai = $_POST['tgl_mulai'];
 $tgl_akhir = $_POST['tgl_akhir'];
$nama = $_SESSION['namalengkap'];
// $w = $th1."-".$tg1;
if($today30 < $today){
		echo "<script>alert('Tanggal akhir tidak boleh sebelum tanggal awal');window.location='../index.php?page=viewaset'</script>";
	}

require('fpdf.php');

$pdf = new FPDF('l','mm','A4');

$pdf->AddPage();

$pdf->SetFont('Arial','B',16);
$pdf->image('logos.png',10,5,20,20);
$pdf->Cell(280,7,'LAPORAN DATA PEMINJAMAN ASET',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(280,7,'PERUMAHAN GRAHA SEGOVIA RW 006',0,1,'C');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(280,4,'DESA CIAKAR KEC.PANONGAN KAB.TANGERANG-BANTEN',0,1,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(280,7,'PRINT BY '.$printby.'',0,1,'L');
$pdf->Cell(280,5,'PRINT DATE '.$date.'',0,2,'L');
 

$pdf->Cell(10,7,'',0,1);
 
$pdf->SetFont('Arial','B',10);
$pdf->Cell(30,6,'ID Peminjaman',1,0);
$pdf->Cell(40,6,'Nama Warga',1,0);
$pdf->Cell(30,6,'Nama Aset',1,0);
$pdf->Cell(30,6,'Tgl Pinjam',1,0);
$pdf->Cell(40,6,'Tgl Jatuh Tempo',1,0);
$pdf->Cell(20,6,'Status',1,0);
$pdf->Cell(35,6,'Jumlah Pinjam',1,0);
$pdf->Cell(30,6,'Username',1,1);
$pdf->SetFont('Arial','',10);
 
$d = $con->query("SELECT tb_peminjaman.id_peminjaman, tb_warga.nama_warga, tb_aset.nama_aset, tb_detailpeminjaman.tgl_pinjam, tb_detailpeminjaman.tgl_jatuhtempo, tb_peminjaman.status_peminjaman, tb_peminjaman.jumlah_peminjaman, tb_detailpeminjaman.username FROM tb_warga INNER JOIN (tb_peminjaman INNER JOIN (tb_aset INNER JOIN tb_detailpeminjaman ON tb_aset.id_aset = tb_detailpeminjaman.id_aset) ON tb_peminjaman.id_peminjaman = tb_detailpeminjaman.id_peminjaman) ON tb_warga.no_kk = tb_peminjaman.no_kk WHERE date(tgl_pinjam) between DATE('$tgl_mulai') AND DATE('$tgl_akhir') ORDER BY tgl_pinjam ASC");
   foreach ($d as $dd) {
	$pdf->Cell(30,6,$dd['id_peminjaman'],1,0);
	$pdf->Cell(40,6,$dd['nama_warga'],1,0);
	$pdf->Cell(30,6,$dd['nama_aset'],1,0);
    $pdf->Cell(30,6,$dd['tgl_pinjam'],1,0); 
	$pdf->Cell(40,6,$dd['tgl_jatuhtempo'],1,0); 
	$pdf->Cell(20,6,$dd['status_peminjaman'],1,0); 
	$pdf->Cell(35,6,$dd['jumlah_peminjaman'],1,0);
	$pdf->Cell(30,6,$dd['username'],1,1);
}
$pdf->SetFont('Arial','',12);
$pdf->Cell(465,18,'Tangerang, '.$date.'',0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->Cell(465,20,''.$nama.'',0,1,'C');
$pdf->Output();
?>