<?php
include ('../include/config.php');
session_start();
$printby = $_SESSION['username'];
$date = DATE('d-m-Y');
$tg1 = $_POST['tg1'];
$th1 = $_POST['th1'];
$w = $th1."-".$tg1;


require('fpdf.php');

$pdf = new FPDF('l','mm','A4');

$pdf->AddPage();

$pdf->SetFont('Arial','B',16);
$pdf->image('logos.png',10,5,25,25);
$pdf->Cell(250,7,'LAPORAN DATA Pembayaran Iuran',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(250,7,'PERUMAHAN GRAHA SEGOVIA RW 006',0,1,'C');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(250,4,'DESA CIAKAR KEC.PANONGAN KAB.TANGERANG-BANTEN',0,1,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(250,7,'PRINT BY '.$printby.'',0,1,'L');
$pdf->Cell(250,5,'PRINT DATE '.$date.'',0,2,'L');
 

$pdf->Cell(10,4,'',0,1);
 
$pdf->SetFont('Arial','B',10);
$pdf->Cell(25,6,'ID Transaksi',1,0);
$pdf->Cell(20,6,'Tanggal',1,0);
$pdf->Cell(30,6,'Nama Warga',1,0);
$pdf->Cell(25,6,'Blok Rumah',1,0);
$pdf->Cell(10,6,'RT',1,0);
$pdf->Cell(30,6,'ID Iuran',1,0);
$pdf->Cell(25,6,'Pembayaran',1,0);
$pdf->Cell(30,6,'Total Harga',1,0);
$pdf->Cell(60,6,'Keterangan',1,1);

$pdf->SetFont('Arial','',10);
 
$d = mysqli_query($con, "SELECT tb_pembayaran.id_transaksi, tb_pembayaran.tanggal, tb_warga.nama_warga, tb_warga.nama_warga, tb_warga.blok_rumah, tb_warga.rt, tb_pembayaran.id_iuran, tb_pembayaran.pembayaran, tb_pembayaran.total_harga, tb_pembayaran.keterangan FROM tb_warga INNER JOIN tb_pembayaran ON tb_warga.no_kk = tb_pembayaran.no_kk WHERE month(tb_pembayaran.tanggal) = '$tg1' AND year(tb_pembayaran.tanggal) = '$th1'");
    while ($dd = mysqli_fetch_array($d)){
    $pdf->Cell(25,6,$dd['id_transaksi'],1,0);
	$pdf->Cell(20,6,$dd['tanggal'],1,0);
    $pdf->Cell(30,6,$dd['nama_warga'],1,0);
    $pdf->Cell(25,6,$dd['blok_rumah'],1,0);
    $pdf->Cell(10,6,$dd['rt'],1,0); 
	$pdf->Cell(30,6,$dd['id_iuran'],1,0); 
	$pdf->Cell(25,6,$dd['pembayaran'],1,0); 
	$uang = number_format($dd['total_harga']);
	$pdf->Cell(30,6,$uang,1,0);
	$pdf->Cell(60,6,$dd['keterangan'],1,1); 
	 
}
 $pdf->SetFont('Arial','',12);
$pdf->Cell(465,18,'Tangerang, '.$date.'',0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->Cell(465,20,''.$printby.'',0,1,'C');
$pdf->Output();
?>