<?php
include ('../include/config.php');
session_start();
$printby = $_SESSION['username'];
$date = DATE('d-m-Y');
$tg1 = $_POST['tg1'];
$th1 = $_POST['th1'];
$nama = $_SESSION['namalengkap'];
$w = $th1."-".$tg1;


require('fpdf.php');

$pdf = new FPDF('l','mm','A5');

$pdf->AddPage();

$pdf->SetFont('Arial','B',16);
$pdf->image('logos.png',10,5,20,20);
$pdf->Cell(190,7,'LAPORAN DATA WARGA',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,7,'PERUMAHAN GRAHA SEGOVIA RW 006',0,1,'C');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(190,4,'DESA CIAKAR KEC.PANONGAN KAB.TANGERANG-BANTEN',0,1,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(190,7,'PRINT BY '.$printby.'',0,1,'L');
$pdf->Cell(190,5,'PRINT DATE '.$date.'',0,2,'L');
 

$pdf->Cell(10,2,'',0,1);
 
$pdf->SetFont('Arial','B',10);
$pdf->Cell(38,6,'NO_KK',1,0);
$pdf->Cell(30,6,'Nama Warga',1,0);
$pdf->Cell(30,6,'Jumlah Keluarga',1,0);
$pdf->Cell(25,6,'NO HP',1,0);
$pdf->Cell(22,6,'Blok rumah',1,0);
$pdf->Cell(8,6,'RT',1,0);
$pdf->Cell(25,6,'Status PAM',1,1);

$pdf->SetFont('Arial','',10);
 
$d = $con->query("SELECT no_kk, nama_warga, jumlah_keluarga, no_hp, blok_rumah, rt, status_pam FROM tb_warga");
    foreach ($d as $dd) {
    $pdf->Cell(38,6,$dd['no_kk'],1,0);
	$pdf->Cell(30,6,$dd['nama_warga'],1,0);
    $pdf->Cell(30,6,$dd['jumlah_keluarga'],1,0);
    $pdf->Cell(25,6,$dd['no_hp'],1,0);
    $pdf->Cell(22,6,$dd['blok_rumah'],1,0); 
	$pdf->Cell(8,6,$dd['rt'],1,0); 
	$pdf->Cell(25,6,$dd['status_pam'],1,1); 
}
$pdf->SetFont('Arial','',12);
$pdf->Cell(310,18,'Tangerang, '.$date.'',0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->Cell(310,20,''.$nama.'',0,1,'C');
$pdf->Output();
?>