<?php
include ('../include/config.php');
session_start();
$printby = $_SESSION['username'];
$date = DATE('d-m-Y');
$type = $_POST['cmbjenis'];
$tg1 = $_POST['tg1'];
$th1 = $_POST['th1'];
$w = $th1."-".$tg1;


require('fpdf.php');

$pdf = new FPDF('l','mm','A5');

$pdf->AddPage();

$pdf->SetFont('Arial','B',16);
 
$pdf->Cell(190,7,'Transaksi',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,7,'REPORT ACCORDING JENIS IURAN AND PERIODE',0,1,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(190,7,'PRINT BY '.$printby.'',0,1,'L');
$pdf->Cell(190,7,'PRINT DATE '.$date.'',0,2,'L');
 

$pdf->Cell(10,7,'',0,1);
 
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'ID Transaksi',1,0);
$pdf->Cell(30,6,'Tanggal',1,0);
$pdf->Cell(30,6,'Nama Warga',1,0);
$pdf->Cell(30,6,'Blok Rumah',1,0);
$pdf->Cell(10,6,'RT',1,0);
$pdf->Cell(30,6,'ID Iuran',1,1);
$pdf->Cell(10,6,'Pembayaran',1,0);
$pdf->Cell(10,6,'Total Harga',1,0);
$pdf->Cell(40,6,'Keterangan',1,0);
$pdf->SetFont('Arial','',10);
 
$d = $con->query("SELECT tb_pembayaran.id_transaksi, tb_pembayaran.tanggal, tb_warga.nama_warga as nama, tb_warga.blok_rumah as blok, tb_warga.rt as rt, tb_pembayaran.id_iuran, tb_pembayaran.pembayaran, tb_pembayaran.total_harga, tb_pembayaran.keterangan FROM tb_warga INNER JOIN tb_pembayaran ON tb_warga.no_kk = tb_pembayaran.no_kk ON tb_iuran.id_iuran = tb_pembayaran.id_iuran) AND month(tb_pembayaran.tanggal) = '$tg1' AND year(tb_pembayaran.tanggal) = '$th1'");
    foreach ($d as $dd) {
    $pdf->Cell(20,6,$dd['id_transaksi'],1,0);
	$pdf->Cell(40,6,$dd['tanggal'],1,0);
    $pdf->Cell(30,6,$dd['nama'],1,0);
    $pdf->Cell(30,6,$dd['blok'],1,0);
    $pdf->Cell(40,6,$dd['rt'],1,0); 
	$pdf->Cell(40,6,$dd['id_iuran'],1,0); 
	$pdf->Cell(40,6,$dd['pembayaran'],1,0); 
	$uang = number_format($dd['total_harga']);
	$pdf->Cell(40,6,$dd['keterangan'],1,0); 
	$pdf->Cell(30,6,$uang,1,1); 
}
 
$pdf->Output();
?>