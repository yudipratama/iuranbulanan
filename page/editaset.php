<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function kodeScript(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;

keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();

// check goodkeys
if (goods.indexOf(keychar) != -1)
	return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
  
if (key == 13) {
	var i;
	for (i = 0; i < field.form.elements.length; i++)
		if (field == field.form.elements[i])
			break;
	i = (i + 1) % field.form.elements.length;
	field.form.elements[i].focus();
	return false;
	};
// else return false
return false;
}
</script>
<?php
	$id = $_GET['id_aset'];
	$r = $con->query("SELECT * FROM tb_aset WHERE id_aset = '$id' ");
	foreach($r as $rr){	
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Master Aset</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Aset</h1>
			</div>
		</div><!--/.row-->
		
		<div class="panel panel-default">
			<div class="panel-heading">Input Here</div>
			<div class="panel-body">
				<div class="col-md-12"">
					<form role="form" action="controler/act_editaset.php" method="post">
						<div class="form-group">
							<label>ID Aset</label>
							<input class="form-control" placeholder="ID Aset" value="<?php echo $rr['id_aset'];?>" name="txtidaset" type="text" required="required" maxlength="8" readonly>
						</div>
						<div class="form-group">
							<label>Nama Aset</label>
							<input class="form-control" placeholder="Nama Aset" type="text" value="<?php echo $rr['nama_aset'];?>" name="txtnamaaset" maxlength="20" required>
						</div>
						<div class="form-group">
							<label>Jumlah</label>
							<input class="form-control" placeholder="Jumlah" name="txtjml" type="text" required="required" readonly onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="3" value="<?php echo $rr['jumlah']?>">
						</div>
						<div class="form-group">
							<label>Total Jumlah</label>
							<input class="form-control" placeholder="Total Jumlah" name="txttotjml" type="text" required="required" readonly onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="3" value="<?php echo $rr['total_jumlah']?>">
						</div>
						<div class="form-group">
							<label>Tambah Jumlah Aset</label>
							<input class="form-control" placeholder="Tambah Jumlah Aset" name="txttambah" type="text" required="required" onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="3">
						</div>
						<div class="form-group">
							<label>Harga</label>
							<input class="form-control" placeholder="Harga" name="txtharga" type="text" required="required" onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="11" value="<?php echo $rr['harga_satuan']?>">
						</div>
							<button type="submit" class="btn btn-primary">Save</button>
							<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
					</div>
				
					</form>
				</div>
			</div>
</div>

<?php
	}
?>