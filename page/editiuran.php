<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function kodeScript(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;

keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();

// check goodkeys
if (goods.indexOf(keychar) != -1)
	return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
  
if (key == 13) {
	var i;
	for (i = 0; i < field.form.elements.length; i++)
		if (field == field.form.elements[i])
			break;
	i = (i + 1) % field.form.elements.length;
	field.form.elements[i].focus();
	return false;
	};
// else return false
return false;
}
</script>
<?php
$id = $_GET['id_iuran'];
$r = $con->query("SELECT * FROM tb_iuran WHERE id_iuran = '$id'");
foreach ($r as $rr) {
	
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
                <li class="active">IURAN</li>
			</ol>
		</div><!--/.row-->
        <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Iuran</h1>
			</div>
		</div><!--/.row-->
        
    <div class="panel panel-default">
                <div class="panel-heading">INPUT HERE</div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <form role="form" action="controler/act_editiuran.php" method="POST">
                            <div class="form-group">
                                 <label>ID IURAN</label>
                                <input class="form-control" name="txtidiuran" placeholder="ID IURAN" type="text" required="required" maxlength="10" value="<?php echo $rr['id_iuran']?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>NAMA IURAN</label>
                               <input class="form-control" name="txtnamaiuran" placeholder="NAMA IURAN" type="text" maxlength="30" required="required" value="<?php echo $rr['nama_iuran']?>" >
                            </div>
                            <label>NOMINAL HARGA</label>
                               <input class="form-control" name="txtharga" placeholder="NOMINAL HARGA" type="text" min="0" required="required" onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="11" value="<?php echo $rr['nominal_harga']?>">
                            </div>
							</div>
                                <button type="submit" class="btn btn-primary">update</button>
                                <a onclick="history.go(-1);return false;" class="btn btn-danger">back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.panel-->
			
</div>
<?php
}
?>
