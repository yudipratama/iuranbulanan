<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function kodeScript(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;

keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();

// check goodkeys
if (goods.indexOf(keychar) != -1)
	return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
  
if (key == 13) {
	var i;
	for (i = 0; i < field.form.elements.length; i++)
		if (field == field.form.elements[i])
			break;
	i = (i + 1) % field.form.elements.length;
	field.form.elements[i].focus();
	return false;
	};
// else return false
return false;
}
</script>
<?php
	$username = $_GET['username'];
	$r = $con->query("SELECT * FROM user WHERE username = '$username'");
	foreach ($r as $rr) {
		
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
            <li>
            <a href="index.php?page=viewuser">User</a>
            </li>
			<li class="active">Edit User</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Master User</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading">Input Here</div>
	<div class="panel-body">
		<div class="col-md-12">
			<form role="form" action="controler/act_updateuser.php?username=<?php echo $username; ?>" method="POST">
				<div class="form-group">
					<label>Username</label>
					<input class="form-control" type="text" value="<?php echo $rr['username'];?>" placeholder="Username" name="txtusername" readonly>
				</div>
				<div class="form-group">
					<label>Nama Lengkap</label>
					<input class="form-control" placeholder="Nama Lengkap" type="text" value="<?php echo $rr['namalengkap'];?>" name="txtnamalengkap" onKeyPress="return kodeScript(event,' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)" maxlength="20" required>
				</div>
                <div class="form-group">
					<label>Authorization</label>
					<select class="form-control" name="cbauthor">
					<option value="Bendahara RW"<?php if($rr['authorization'] == "Bendahara RW"){echo "selected";}?>>Bendahara RW</option>
                    <option value="Petugas"<?php if($rr['authorization'] == "Petugas"){echo "selected";}?>>Petugas</option>
					<option value="Ketua RW"<?php if($rr['authorization'] == "Ketua RW"){echo "selected";}?>>Ketua RW</option>
					<option value="Sekretaris RW"<?php if($rr['authorization'] == "Sekretaris RW"){echo "selected";}?>>Sekretaris RW</option>
					<option value="Warga"<?php if($rr['authorization'] == "Warga"){echo "selected";}?>>Warga</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
	}
?>