<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Ganti Password</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Ganti Password</h1>
		</div>
	</div><!--/.row-->
<div class="panel panel-default">
	<div class="panel-heading">Input Here</div>
	<div class="panel-body">
		<div class="col-md-12">
			<form role="form" action="controler/act-gantipassword.php" method="POST">
				<div class="form-group">
					<label>Password Lama</label>
					<input class="form-control" type="password" name="pwdlama" id="pwdlama" maxlength="25" required>
				</div>
				<div class="form-group">
					<label>Password Baru</label>
					<input class="form-control" type="password" name="pwdbaru" id="pwdbaru" title="8 Karakter Password, Gunakan Huruf besar,Kecil dan Angka" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" maxlength="25" required>
				</div>
                <div class="form-group">
					<label>Ketik Ulang Password Baru</label>
					<input class="form-control" type="password" name="pwd2" id="pwd2" maxlength="25" required>
				</div>
				<button type="submit" class="btn btn-primary">Simpan</button>
				<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
				</div>
			</form>
		</div>
	</div>
</div>