
<div id="content">
	<div id="content-header">
	  <div id="breadcrumb"> <a href="index.php?page=home" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="index.php?page=gantipassword" class="tip-bottom">Ganti Password</a> </div>
	  <h1>Ganti Password</h1>
	</div>
	<div class="container-fluid">
	<hr>
	
	<div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Ganti Password</h5>
            </div>
            <div class="widget-content nopadding">
              <form class="form-horizontal" method="post" action="controler/act-gantipassword.php" name="password_validate" id="password_validate" novalidate="novalidate">
                <div class="control-group">
                  <label class="control-label">Password Lama</label>
                  <div class="controls">
                    <input type="password" name="pwdlama" id="pwdlama" />
                  </div>
                </div>
				<div class="control-group">
                  <label class="control-label">Password Baru</label>
                  <div class="controls">
                    <input type="password" name="pwdbaru" id="pwdbaru" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Ketik Ulang Password Baru</label>
                  <div class="controls">
                    <input type="password" name="pwd2" id="pwd2" />
                  </div>
                </div>
                <div class="form-actions">
                  <input type="submit" value="Simpan" class="btn btn-success">
				  <input type="reset" value="Atur Kembali" class="btn btn-primary">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
	
 </div>
</div>