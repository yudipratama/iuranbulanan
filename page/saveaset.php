<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function kodeScript(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;

keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();

// check goodkeys
if (goods.indexOf(keychar) != -1)
	return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
  
if (key == 13) {
	var i;
	for (i = 0; i < field.form.elements.length; i++)
		if (field == field.form.elements[i])
			break;
	i = (i + 1) % field.form.elements.length;
	field.form.elements[i].focus();
	return false;
	};
// else return false
return false;
}
</script>
<?php
	$carikode = $con->query("SELECT id_aset FROM tb_aset") or die (mysql_error());
	$datakode = mysqli_fetch_array($carikode);
	$jumlah_data = mysqli_num_rows($carikode);
			if ($datakode){
			$nilaikode = substr($jumlah_data[0],1);
			$kode = (int) $nilaikode;
			$kode = $jumlah_data + 1;
			$kode_otomatis = "ASET".str_pad($kode, 3, "0", STR_PAD_LEFT);
		}else{
			$kode_otomatis="ASET001";
		}
		?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Master Aset</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Aset</h1>
			</div>
		</div><!--/.row-->
		
		<div class="panel panel-default">
			<div class="panel-heading">Input Here</div>
			<div class="panel-body">
				<div class="col-md-12"">
					<form role="form" action="controler/act_saveaset.php" method="post">
						<div class="form-group">
							<label>ID Aset</label>
							<input class="form-control" placeholder="ID Aset" name="txtidaset" type="text" required="required" value="<?php echo "$kode_otomatis" ?>" readonly>
						</div>
						<div class="form-group">
							<label>Nama Aset</label>
							<input class="form-control" placeholder="Nama Aset" name="txtnamaaset" type="text" maxlength="20" required>
						</div>
						<div class="form-group">
							<label>Jumlah</label>
							<input class="form-control" placeholder="Jumlah" name="txtjml" type="text" onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="3" required>
						</div>
						<div class="form-group">
							<label>Harga Satuan</label>
							<input class="form-control" placeholder="Harga" name="txtharga" type="text" onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="11" required>
						</div>
							<button type="submit" class="btn btn-primary">Save</button>
							<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
					</div>
				
					</form>
				</div>
			</div>
</div>