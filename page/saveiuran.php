<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function kodeScript(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;

keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();

// check goodkeys
if (goods.indexOf(keychar) != -1)
	return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
  
if (key == 13) {
	var i;
	for (i = 0; i < field.form.elements.length; i++)
		if (field == field.form.elements[i])
			break;
	i = (i + 1) % field.form.elements.length;
	field.form.elements[i].focus();
	return false;
	};
// else return false
return false;
}
</script>
<?php
	$carikode = $con->query("SELECT id_iuran FROM tb_iuran") or die (mysql_error());
	$datakode = mysqli_fetch_array($carikode);
	$jumlah_data = mysqli_num_rows($carikode);
			if ($datakode){
			$nilaikode = substr($jumlah_data[0],1);
			$kode = (int) $nilaikode;
			$kode = $jumlah_data + 1;
			$kode_otomatis = "IURAN".str_pad($kode, 2, "0", STR_PAD_LEFT);
		}else{
			$kode_otomatis="IURAN01";
		}
		?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
                <li class="active">IURAN</li>
			</ol>
		</div><!--/.row-->
        <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Iuran</h1>
			</div>
		</div><!--/.row-->
        
    <div class="panel panel-default">
                <div class="panel-heading">INPUT HERE</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <form role="form" action="controler/act_saveiuran.php" method="POST">
                             <div class="form-group">
                                <label>ID IURAN</label>
                                <input class="form-control" name="txtidiuran" placeholder="ID IURAN" type="text" required="required" maxlength="10" value="<?php echo "$kode_otomatis" ?>" readonly>
                            </div>
                           <div class="form-group">
                                <label>NAMA IURAN</label>
                                <input class="form-control" name="txtnamaiuran" placeholder="NAMA IURAN" type="text" maxlength="30" required="required">
                            </div>
                            <div class="form-group">
                                <label>NOMINAL HARGA</label>
                                <input class="form-control" name="txtharga" placeholder="NOMINAL HARGA" type="text" maxlength="11" onKeyPress="return kodeScript(event,'0123456789',this)" required="required">
                            </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                 <a onclick="history.go(-1);return false;" class="btn btn-danger">back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.panel-->
</div>