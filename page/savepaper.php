<?php
	$carikode = $con->query("SELECT MAX(kode_paper) FROM tb_paper");
	$datakode = mysqli_fetch_array($carikode);
		if ($datakode){
			$nilaikode = substr($datakode[0],1);
			$kode = (int) $nilaikode;
			$kode = $kode+1;
			$kode_otomatis = "P".str_pad($kode, 2, "0", STR_PAD_LEFT);
		}else{
			$kode_otomatis="P01";
		}
		?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
                <li class="active">ROLL PAPER</li>
			</ol>
		</div><!--/.row-->
        <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master ROLL PAPER</h1>
			</div>
		</div><!--/.row-->
        
    <div class="panel panel-default">
                <div class="panel-heading">INPUT HERE</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <form role="form" action="controler/act_savepaper.php" method="POST">
                            <div class="form-group">
                                <label>KODE PAPER</label>
                                <input class="form-control" name="txtkdpaper" placeholder="ID ROOMS" type="text" required="required" maxlength="3" value="<?php echo "$kode_otomatis" ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>TIPE PAPER</label>
                                <select class="form-control" name="cmbtipe">
								<?php
								$r = $con->query("SELECT * FROM tipe");
								foreach ($r as $rr) {
									?>
                                	<option value="<?php echo $rr['tipe_paper'];?>"><?php echo $rr['tipe_paper'];?></option>
									<?php
									}
									?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>TINGGI ROLL</label>
                                <input class="form-control" name="txttgroll" placeholder="TINGGI ROLL" min="0" type="number" required="required">
                            </div>
                            <div class="form-group">
                                <label>JUMLAH ROLL</label>
                                 <input class="form-control" name="txtjmlroll" placeholder="JUMLAH ROLL" min="0" type="number" required="required">
                            </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                 <a onclick="history.go(-1);return false;" class="btn btn-danger">back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.panel-->
</div>