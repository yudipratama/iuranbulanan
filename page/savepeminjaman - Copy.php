<?php
$carikode = $con->query("SELECT MAX(id_peminjaman) FROM tb_peminjaman");
$datakode = mysqli_fetch_array($carikode);
	if ($datakode) {
		$nilaikode = substr($datakode[0], 2);
   		$kode = (int) $nilaikode;
   		$kode = $kode + 1;
   		$kode_otomatis = "PEM".str_pad($kode, 4, "0", STR_PAD_LEFT);
   	} else {
   		$kode_otomatis = "PEM0001";
  	}
	  // Simpan daftar barang
    if (isset($_POST['btn_add'])){
        $id_aset = $_POST['id_aset'];
        $nama_aset = $_POST['nama_aset'];
        $jatuh_tempo = $_POST['tgl_jatuhtempo'];
		$username = $_SESSION['username'];
        if ($nama_aset == "") {
            echo "<script>alert('Barang tidak terdaftar')</script>";
        } else {
            $con->query("INSERT INTO penjualan_details_temp values ('$kode_otomatis', '$id_aset', '$nama_aset', '$tgl_jatuhtempo', '$username')");

            if ($con->affected_rows > 0){
                // Respon berhasil tidak ada
            }else{
                echo "<script>alert('Barang sudah ada dalam list')</script>";
            }
        }
    }

    // Hapus daftar barang
    if (isset($_POST['btn_del'])){
        $id_aset = $_POST['btn_del'];

        $con->query("DELETE FROM tb_detailpeminjaman_temp WHERE id_aset='$id_aset'");
    }
	
    // Hitung Total Qty
    $result = $con->query("SELECT SUM(*) FROM tb_detailpeminjaman_temp");
    $row = $result->fetch_row();
    $total_qty = $row[0];
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Peminjaman Aset</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Peminjaman Aset</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading">Input Here</div>
	<div class="panel-body">
		<form role="form" action="controler/act_savepeminjaman.php" method="POST" id="formtransaksi">
			<div class="form-group col-md-9">
				<label>ID Peminjaman</label>
				<input class="form-control " placeholder="ID Peminjaman" type="text" name="txtidpeminjaman" value="<?php echo "$kode_otomatis"; ?>" readonly required>
			</div>
			<div class="form-group col-md-3">
				<label>NO KK</label>
				<input class="form-control txtnokk" placeholder="NO KK" type="number" onkeyup="isi_otomatis()" min="0" name="txtnokk" id="txtnokk" required>
			</div>
			<div class="form-group col-md-3">
				<label>Nama Warga</label>
				<input class="form-control" placeholder="Nama Warga" type="text" name="txtnamawarga" id="txtnamawarga" required readonly>
			</div>
			<div class="form-group col-md-3">
				<label>Blok Rumah</label>
				<input class="form-control" placeholder="Blok Rumah" type="text" name="txtblok" id="txtblok" required readonly>
			</div>
			<div class="form-group col-md-3">
				<label>RT</label>
				<input class="form-control" placeholder="RT" type="text" name="txtrt" id="txtrt" required readonly>
			</div>
			<div class="form-group col-md-6">
				<label>Status Peminjaman</label>
				<input class="form-control txtstpeminjaman" placeholder="Status Peminjaman" type="text" name="txtstpeminjaman" id="txtpembayaran" required>
			</div>
			<div class="form-group col-md-2">
				<label>Jumlah Peminjaman</label>
				<input class="form-control" placeholder="Jumlah Peminjaman" type="number" name="txtjmlpem" id="txtjmlpem" required>
			</div>
			 <form action="" method="POST">

                            <div class="form-row">
                                <div class="col form-group">
                                    <input type="text" class="form-control" name="id_barang" id="id_barang" onkeyup="isi_otomatis_barang()" onchange="isi_otomatis_barang()" required>
                                    <label>ID Barang</label>
                                </div>
                                <div class="col-7 form-group">
                                    <input type="text" class="form-control" name="nama_barang" id="nama_barang" readonly required>
                                    <!-- <label>Nama Barang</label> -->
                                </div>
                                <div class="col-2 form-group">
                                    <input type="text" class="form-control" name="harga_jual" id="harga_jual" readonly required>
                                    <!-- <label>Harga</label> -->
                                </div>
                                <div class="col-1 form-group">
                                    <input type="text" class="form-control" name="qty" onkeypress="return OnlyNumber(event)" required>
                                    <label>QTY</label>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" name="btn_add" value="1" class="btn btn-secondary btn-fab btn-sm">
                                        <i class="icon-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
						 <div class="form-row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Kode Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Qty</th>
                                        <th>Harga</th>
                                        <th>Total Harga</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $r = $con->query("SELECT * FROM tb_detailpeminjaman_temp");
                                        while ($rr = $r->fetch_array()) {
                                    ?>
                                    <tr class="table-warning">
                                        <td><?php echo $rr['id_aset'];?></td>
                                        <td><?php echo $rr['nama_aset'];?></td>
										<td><?php echo $rr['tgl_jatuhtempo'];?></td>
                                        <td><?php echo $rr['tgl_jatuhtempo'];?></td>
                                        <td><?php echo $rr['username'];?></td>
                                        <td class="text-center">
                                        <form action="" method="POST">
                                            <button type="submit" name="btn_del" value="<?php echo $rr['id_aset'];?>" class="btn btn-danger btn-fab btn-sm">
                                                <i class="icon-trash"></i>
                                            </button>
                                        </form>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
			<div class="col-md-12">
				<button type="submit" class="btn btn-primary">Save</button>
				<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
			</div>
		</form>
	</div>
</div>