<?php
	$carikode = $con->query("SELECT id_peminjaman FROM tb_peminjaman") or die (mysql_error());
	$datakode = mysqli_fetch_array($carikode);
	$jumlah_data = mysqli_num_rows($carikode);
			if ($datakode){
			$nilaikode = substr($jumlah_data[0],1);
			$kode = (int) $nilaikode;
			$kode = $jumlah_data + 1;
			$kode_otomatis = "PEM".str_pad($kode, 4, "0", STR_PAD_LEFT);
		}else{
			$kode_otomatis="PEM0001";
		}
	  // Simpan daftar barang
    if (isset($_POST['btn_add'])){
        $id_aset = $_POST['txtidaset'];
        $nama_aset = $_POST['txtnmaset'];
		$username = $_SESSION['username'];
		$today = date("Y-m-d");
		$today3 = date("Y-m-d", strtotime($today . "+3 day"));
        if ($nama_aset == "") {
            echo "<script>alert('Barang tidak terdaftar')</script>";
        } else {
            $con->query("INSERT INTO tb_detailpeminjaman_temp VALUES('$kode_otomatis', '$id_aset', '$nama_aset', '$today', '$today3', '$username')");

            if ($con->affected_rows > 0){
                // Respon berhasil tidak ada
            }else{
                echo "<script>alert('Barang sudah ada dalam list')</script>";
            }
        }
    }
    // Hapus daftar barang
    if (isset($_POST['btn_del'])){
        $id_aset = $_POST['btn_del'];

        $con->query("DELETE FROM tb_detailpeminjaman_temp WHERE id_aset='$id_aset'");
    }
	
    // Hitung Total Qty
    $result = $con->query("SELECT count(*) FROM tb_detailpeminjaman_temp");
    $row = $result->fetch_row();
    $jumlah_peminjaman = $row[0];
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Peminjaman Aset</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Peminjaman Aset</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading">Input Here</div>
	<div class="panel-body">
				 <form action="" method="POST">

					<div class="form-row">
						<div class="form-group col-md-5">
							<label>ID Aset</label>
							<input type="text" class="form-control txtidaset" name="txtidaset" id="txtidaset" onkeyup="isi_otomatis3()" onchange="isi_otomatis3()" required>      
						</div>
						<div class="form-group col-md-5">
							<label>Nama Aset</label>
							<input type="text" class="form-control" name="txtnmaset" id="txtnmaset" readonly required>
							<!-- <label>Nama Aset</label> -->
						</div>
					   <div class="form-group text-center">
							<button type="submit" name="btn_add" value="1" class="btn btn-primary">Tambah</button>
					</div>
                    </form>
						 <div class="form-row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Id aset</th>
                                        <th>Nama Aset</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Tangggal JatuhTempo</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $r = $con->query("SELECT * FROM tb_detailpeminjaman_temp");
                                        while ($rr = $r->fetch_array()) {
											
                                    ?>
                                    <tr class="table-warning">
                                        <td><?php echo $rr['id_aset'];?></td>
                                        <td><?php echo $rr['nama_aset'];?></td>
										<td><?php echo date("Y-m-d", strtotime($rr['tgl_pinjam']));?></td>
                                        <td><?php echo date("Y-m-d", strtotime($today . "+3 day"));?></td>
                                        <td class="text-center">
                                        <form action="" method="POST">
                                            <button type="submit" name="btn_del" value="<?php echo $rr['id_aset'];?>" class="btn btn-danger btn-fab btn-sm">delete</button>
                                        </form>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
		<form role="form" action="controler/act_savepeminjaman.php" method="POST" id="formtransaksi">
			<div class="form-group col-md-12">
				<label>ID Peminjaman</label>
				<input class="form-control " placeholder="ID Peminjaman" type="text" name="txtidpeminjaman" id="txtidpeminjaman" value="<?php echo "$kode_otomatis"; ?>" readonly required>
			</div>
			<div class="form-group col-md-3">
				<label>NO KK</label>
				<input class="form-control txtnokk" placeholder="NO KK" type="number" onkeyup="isi_otomatis()" onchange="isi_otomatis()" min="0" name="txtnokk" id="txtnokk" required>
			</div>
			<div class="form-group col-md-3">
				<label>Nama Warga</label>
				<input class="form-control" placeholder="Nama Warga" type="text" name="txtnamawarga" id="txtnamawarga" required readonly>
			</div>
			<div class="form-group col-md-3">
				<label>Blok Rumah</label>
				<input class="form-control" placeholder="Blok Rumah" type="text" name="txtblok" id="txtblok" required readonly>
			</div>
			<div class="form-group col-md-3">
				<label>RT</label>
				<input class="form-control" placeholder="RT" type="text" name="txtrt" id="txtrt" required readonly>
			</div>
			<div class="col-md-12">
				<button type="submit" class="btn btn-primary">Save</button>
				<a href="index.php?page=viewpeminjaman" class="btn btn-danger">Back</a>
			</div>
		</form>
	</div>
</div>