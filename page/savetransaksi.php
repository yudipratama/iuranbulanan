<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function kodeScript(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;

keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();

// check goodkeys
if (goods.indexOf(keychar) != -1)
	return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
  
if (key == 13) {
	var i;
	for (i = 0; i < field.form.elements.length; i++)
		if (field == field.form.elements[i])
			break;
	i = (i + 1) % field.form.elements.length;
	field.form.elements[i].focus();
	return false;
	};
// else return false
return false;
}
</script>
<?php
$carikode = $con->query("SELECT MAX(id_transaksi) FROM tb_pembayaran");
$datakode = mysqli_fetch_array($carikode);
	if ($datakode) {
		$nilaikode = substr($datakode[0], 2);
   		$kode = (int) $nilaikode;
   		$kode = $kode + 1;
   		$kode_otomatis = "TR".str_pad($kode, 4, "0", STR_PAD_LEFT);
   	} else {
   		$kode_otomatis = "TR0001";
  	}
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Pembayaran Iuran</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Pembayaran Iuran</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading">Input Here</div>
	<div class="panel-body">
		<form role="form" action="controler/act_savetransaksi.php" method="POST" id="formtransaksi">
			<div class="form-group col-md-9">
				<label>ID Transaksi</label>
				<input class="form-control " placeholder="ID Transaksi" type="text" name="txtidtransaksi" value="<?php echo "$kode_otomatis"; ?>" readonly required>
			</div>
			<div class="form-group col-md-2">
				<label>Tanggal</label>
				<input class="form-control txttanggal" type="date" name="txttanggal" required>
			</div>
			<div class="form-group col-md-3">
				<label>NO KK</label>
				<input class="form-control txtnokk" placeholder="NO KK" type="text" onkeyup="isi_otomatis()" onchange="isi_otomatis()" min="0" name="txtnokk" id="txtnokk" onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="16" required>
			</div>
			<div class="form-group col-md-3">
				<label>Nama Warga</label>
				<input class="form-control" placeholder="Nama Warga" type="text" name="txtnamawarga" id="txtnamawarga" required readonly>
			</div>
			<div class="form-group col-md-3">
				<label>Blok Rumah</label>
				<input class="form-control" placeholder="Blok Rumah" type="text" name="txtblok" id="txtblok" required readonly>
			</div>
			<div class="form-group col-md-3">
				<label>Status PAM</label>
				<input class="form-control" placeholder="Status PAM" type="text" name="txtstatus" id="txtstatus" required readonly>
			</div>
			<div class="form-group col-md-3">
				<label>RT</label>
				<input class="form-control" placeholder="RT" type="text" name="txtrt" id="txtrt" required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>ID Iuran</label>
				<input class="form-control txtidiuran" placeholder="Id Iuran" type="text" onkeyup="isi_otomatis1()" onchange="isi_otomatis1()" name="txtidiuran" id="txtidiuran" maxlength="10"  required>
			</div>
			<div class="form-group col-md-2">
				<label>Nama Iuran</label>
				<input class="form-control" placeholder="Nama Iuran" type="text" name="txtnmiuran" id="txtnmiuran" required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Nominal Harga</label>
				<input class="form-control" placeholder="Nominal Harga" type="text" name="txtnominal" id="txtnominal" required readonly>
			</div>
			<div class="form-group col-md-12">
				<label>Keterangan</label>
				<textarea class="form-control" rows="2" name="txtketerangan" id="txtketerangan" maxlength="30" required></textarea>
			</div>
			<div class="form-group col-md-6">
				<label>Pembayaran</label>
				<input class="form-control txtpembayaran" placeholder="Pembayaran" type="text" onkeyup="isi_otomatis2()" name="txtpembayaran" id="txtpembayaran" onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="2" required>
			</div>
			<div class="col-md-12">
				<button type="submit" class="btn btn-primary">Save</button>
				<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
			</div>
		</form>
	</div>
</div>