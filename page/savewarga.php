<script language="javascript">
function getkey(e)
{
if (window.event)
   return window.event.keyCode;
else if (e)
   return e.which;
else
   return null;
}
function kodeScript(e, goods, field)
{
var key, keychar;
key = getkey(e);
if (key == null) return true;

keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();
goods = goods.toLowerCase();

// check goodkeys
if (goods.indexOf(keychar) != -1)
	return true;
// control keys
if ( key==null || key==0 || key==8 || key==9 || key==27 )
   return true;
  
if (key == 13) {
	var i;
	for (i = 0; i < field.form.elements.length; i++)
		if (field == field.form.elements[i])
			break;
	i = (i + 1) % field.form.elements.length;
	field.form.elements[i].focus();
	return false;
	};
// else return false
return false;
}
</script>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Master Warga</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Warga</h1>
			</div>
		</div><!--/.row-->
		
		<div class="panel panel-default">
			<div class="panel-heading">Input Here</div>
			<div class="panel-body">
				<div class="col-md-12"">
					<form role="form" action="controler/act_savewarga.php" method="post">
						<div class="form-group">
							<label>NO KK</label>
							<input class="form-control" placeholder="NO KK" name="txtnokk" onKeyPress="return kodeScript(event,'0123456789',this)" type="text" maxlength="16" required>
						</div>
						<div class="form-group">
							<label>Nama Warga</label>
							<input class="form-control" placeholder="Nama Warga" name="txtnamawrg" id="txtnamawrg" type="text" maxlength="30" onKeyPress="return kodeScript(event,' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',this)" required>
						</div>
						<div class="form-group">
							<label>Jumlah Keluarga</label>
							<input class="form-control" placeholder="Jumlah Keluarga" name="txtjmlklg" id="txtjmlklg" type="text" onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="2" required>
						</div>
						<div class="form-group">
							<label>No HP</label>
							<input class="form-control" placeholder="No HP" name="txtnohp" id="txtnohp" type="text" onKeyPress="return kodeScript(event,'0123456789',this)" maxlength="13" required>
						</div>
						<div class="form-group">
							<label>Blok Rumah</label>
							<input class="form-control" placeholder="Blok Rumah" name="txtblokrmh" id="txtblokrmh" maxlength="10" type="text" required>
						</div>
						<div class="form-group">
                                <label>RT</label>
                                <select class="form-control" name="cmbrt">
                                	<option value="001">001</option>
									<option value="002">002</option>
									<option value="003">003</option>
									<option value="004">004</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="cmbstatus">
                                	<option value="1">PAM</option>
									<option value="0">NON PAM</option>
                                </select>
                            </div>
							<button type="submit" class="btn btn-primary">Save</button>
							<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
					</div>
				
					</form>
				</div>
			</div>
</div>