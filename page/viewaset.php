<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Aset</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Master Aset</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading"><a href="index.php?page=saveaset" class="btn btn-primary">Tambah</a></div>
	<div class="panel-body">
		<div class="col-md-12">
			<div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>ID Aset</th>
						<th>Tanggal Beli</th>
                        <th>Nama Aset</th>
                        <th>Jumlah</th>
						<th>Harga Satuan</th>
						<th>Total Harga</th>
                        <th>Actions</th>
                    </tr>
                    <?php
                    $r = $con->query("SELECT * FROM tb_aset");
                    while ($rr = $r->fetch_array()) {
                        ?>
                        <tr>
                            <td><?php echo $rr['id_aset'];?></td>
							<td><?php echo $rr['tanggal_beli'];?></td>
                            <td><?php echo $rr['nama_aset'];?></td>
                            <td><?php echo $rr['jumlah'];?></td>
							<td><?php echo $rr['harga_satuan'];?></td>
							<td><?php echo $rr['total_harga_aset'];?></td>
                            <td><a href="index.php?page=editaset&id_aset=<?php echo $rr['id_aset'];?>" class="btn btn-sm btn-success">Edit</a> <a href="controler/act_delaset.php?id_aset=<?php echo $rr['id_aset'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin hapus data ?')"> Hapus</a></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
		</div>
	</div>
</div>