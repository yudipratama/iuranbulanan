<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
                <li class="active">IURAN</li>
			</ol>
		</div><!--/.row-->
        <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Iuran</h1>
			</div>
		</div><!--/.row-->
        
    <div class="panel panel-default">
                <div class="panel-heading"><a href="index.php?page=saveiuran" class="btn btn-primary">Tambah</a></div>
                <div class="panel-body">
                    <div class="col-md-12">
					<div class="table-responsive">
					
					<table class="table table-striped table-condensen">
					<tr>
					<th>ID IURAN</th>
					<th>NAMA IURAN</th>
					<th>NOMINAL HARGA</th>
					<th>ACTION</th>
					</tr>
					<?php
					$r = $con->query("SELECT * FROM tb_iuran");
					while ($rr = $r->fetch_array()){
						?>
						<tr>
						<td><?php echo $rr['id_iuran'];?></td>
						<td><?php echo $rr['nama_iuran'];?></td>
						<td>Rp. <?php echo $rr['nominal_harga'];?></td>
						<td><a class="btn btn-sm btn-primary" href="index.php?page=editiuran&id_iuran=<?php echo $rr['id_iuran'];?>">Edit</a> <a class="btn btn-sm btn-danger" href="controler/act_deliuran.php?id_iuran=<?php echo $rr['id_iuran'];?>" onclick="return confirm('Yakin ingin hapus data')">Hapus</a></td>
						</tr>
						<?php
					}
				?>	
					</table>
                    </div>
					</div>
                </div>
            </div><!-- /.panel-->
</div>