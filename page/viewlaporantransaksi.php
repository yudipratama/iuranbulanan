<?php
    $today = date("Y-m-d");
    $today30 = date('Y-m-d', strtotime("-30 day"));
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Laporan Pembayaran Iuran</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Laporan Pembayaran Iuran</h1>
			</div>
		</div><!--/.row-->
		
		<div class="panel panel-default">
		<div class="panel-heading">
			Choose Type Report
		</div>
			<div class="panel-body">
			<form class="form-horizontal" action="laporan/laporantransaksi1.php" method="post" target="_blank">
                <div class="form-group">
                    <label for="" class="control-label col-sm-1">Jenis Iuran</label>
                    <div class="col-sm-7">
                       <select class="form-control" name="cmbjenis">
								<option value="All">Semua</option>
								<?php
								$r = $con->query("SELECT DISTINCT nama_iuran FROM tb_iuran");
								foreach ($r as $rr) {
									?>
                                	<option value="<?php echo $rr['nama_iuran'];?>"><?php echo $rr['nama_iuran'];?></option>
									<?php
									}
									?>
                                </select>
                    </div>
                </div>
                </div>
			<div class="panel-body">
				<div class="col-sm-11"">
				 <form class="form-horizontal" action="laporan/laporantransaksi1.php" method="post" target="_blank">
                <div class="form-group">
                    <label for="" class="control-label col-sm-1">Periode</label>
                    <div class="col-sm-7">
                     <div class="card-body">
                                    <div class="form-group col-md-6">
										<label>Tanggal Awal</label>
										<input type="date" class="form-control"  value="<?php echo $today30 ?>" name="tgl_mulai"/>
										</div>
										<div class="form-group col-md-6">
										<label>Tanggal Akhir</label>
										<input type="date" class="form-control"  value="<?php echo $today ?>" name="tgl_akhir"/>
										</div>
                    </div>
                </div>
				</div>
                <div class="form-group">
                    <div class="btn-group col-sm-9 col-sm-offset-1">
                        <button class="btn btn-primary" type="submit" >Tampilkan</button>
                        <button class="btn btn-primary" type="reset" >Reset</button>
                    </div>
                </div>
            </form>
			</div>
		</div>