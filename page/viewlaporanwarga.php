<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Laporan Data Warga</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Laporan Data Warga</h1>
			</div>
		</div><!--/.row-->
		
		<div class="panel panel-default">
		<div class="panel-heading">
			Pilih Tipe Laporan
		</div>
			<div class="panel-body">
			<form class="form-horizontal" action="laporan/laporanwarga2.php" method="post" target="_blank">
                <div class="form-group">
                    <label for="" class="control-label col-sm-1">RT</label>
                    <div class="col-sm-11">
                       <select class="form-control" name="cmbjenis">
								<option value="All">Semua</option>
								<?php
								$r = $con->query("SELECT DISTINCT rt FROM tb_warga");
								foreach ($r as $rr) {
									?>
                                	<option value="<?php echo $rr['rt'];?>"><?php echo $rr['rt'];?></option>
									<?php
									}
									?>
                                </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="btn-group col-sm-9 col-sm-offset-1">
                        <button class="btn btn-primary" type="submit" >Tampilkan</button>
                        <button class="btn btn-primary" type="reset" >Reset</button>
                    </div>
                </div>
            </form>
			</div>
		</div>