<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
                <li class="active">Rooms</li>
			</ol>
		</div><!--/.row-->
        <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Paper</h1>
			</div>
		</div><!--/.row-->
        
    <div class="panel panel-default">
                <div class="panel-heading"><a href="index.php?page=savepaper" class="btn btn-primary">Add</a></div>
                <div class="panel-body">
                    <div class="col-md-12">
					<div class="table-responsive">
					
					<table class="table table-striped table-condensen">
					<tr>
					<th>ID PAPER</th>
					<th>TYPE PAPER</th>
					<th>TINGGI ROLL</th>
					<th>JUMLAH ROLL</th>
					</tr>
					<?php
					$r = $con->query("SELECT tb_paper.kode_paper as kode, tipe.tipe_paper as tipe, tb_paper.tinggi_roll as tinggi, tb_paper.jumlah_roll as jumlah FROM tb_paper INNER JOIN tipe ON tb_paper.tipe_paper = tipe.tipe_paper");
					while ($rr = $r->fetch_array()){
						?>
						<tr>
						<td><?php echo $rr['kode'];?></td>
						<td><?php echo $rr['tipe'];?></td>
						<td><?php echo $rr['tinggi'];?></td>
						<td><?php echo $rr['jumlah'];?></td>
						<td><a class="btn btn-sm btn-primary" href="index.php?page=editrooms&id_room=<?php echo $rr['id'];?>">Edit</a> <a class="btn btn-sm btn-danger" href="controler/act_delrooms.php?id_room=<?php echo $rr['id'];?>" onclick="return confirm('Yakin ingin hapus data')">Hapus</a></td>
						</tr>
						<?php
					}
				?>	
					</table>
                    </div>
					</div>
                </div>
            </div><!-- /.panel-->
</div>