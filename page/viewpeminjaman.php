<?php
 if (isset($_POST['btn_tambah'])){
	 $con->query("DELETE FROM tb_detailpeminjaman_temp");
	 if ($con){
		echo "<script>window.location='index.php?page=savepeminjaman'</script>";
	 }
 }
 ?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
                <li class="active">Peminjaman Aset</li>
			</ol>
		</div><!--/.row-->
        <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Peminjaman Aset</h1>
			</div>
		</div><!--/.row-->
        
    <div class="panel panel-default">
                <div class="panel-heading">
				<form action="" method="POST">
					<button type="submit" name="btn_tambah" value="1" class="btn btn-primary">Tambah</button>
				</form>
				</div>
                <div class="panel-body">
                    <div class="col-md-12">
					<div class="table-responsive">
					
					<table class="table table-striped table-condensen">
					<tr>
				     <th>ID Peminjaman</th>
					 <th>No KK</th>
					 <th>Status Peminjaman</th>
					 <th>Jumlah Peminjaman</th>
					</tr>
					<?php
					$r = $con->query("SELECT * FROM tb_peminjaman where status='0'");
					while ($rr = $r->fetch_array()){
						?>
						<tr>
						<td><?php echo $rr['id_peminjaman'];?></td>
						<td><?php echo $rr['no_kk'];?></td>
						<td><?php echo $rr['status_peminjaman'];?></td>
						<td><?php echo $rr['jumlah_peminjaman'];?></td>
						</tr>
						<?php
					}
				?>	
					</table>
                    </div>
					</div>
                </div>
            </div><!-- /.panel-->
</div>