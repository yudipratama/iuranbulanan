<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Pengembalian</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Pengembalian</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading">Choose List Pengembalian</div>
	<div class="panel-body">
		<div class="col-md-12">
			<div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>ID Peminjaman</th>
                        <th>Nama Warga</th>
                        <th>Blok Rumah</th>
						<th>RT</th>
						<th>Status</th>
						<th>Jumlah Peminjaman</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    $r = $con->query("SELECT tb_peminjaman.id_peminjaman as id, tb_warga.nama_warga as nama, tb_warga.blok_rumah as blok, tb_warga.rt as rt, tb_peminjaman.status_peminjaman as status, tb_peminjaman.jumlah_peminjaman as jumlah FROM tb_peminjaman INNER JOIN tb_warga ON tb_peminjaman.no_kk = tb_warga.no_kk WHERE tb_peminjaman.status = 0");
                    while ($rr = $r->fetch_array()) {
                        ?>
                        <tr>
                            <td><?php echo $rr['id'];?></td>
                            <td><?php echo $rr['nama'];?></td>
                            <td><?php echo $rr['blok'];?></td>
                            <td><?php echo $rr['rt'];?></td>
                            <td><?php echo $rr['status'];?></td>
							<td><?php echo $rr['jumlah'];?></td>
					<td>[<a href="index.php?page=savepengembalian&id_peminjaman=<?php echo $rr['id'];?>">Kembalikan</a>]</td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
		</div>
	</div>
</div>