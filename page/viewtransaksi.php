<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
                <li class="active">Pembayaran Iuran</li>
			</ol>
		</div><!--/.row-->
        <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Pembayaran Iuran</h1>
			</div>
		</div><!--/.row-->
        
    <div class="panel panel-default">
                <div class="panel-heading"><a href="index.php?page=savetransaksi" class="btn btn-primary">Tambah</a></div>
                <div class="panel-body">
                    <div class="col-md-12">
					<div class="table-responsive">
					
					<table class="table table-striped table-condensen">
					<tr>
				     <th>ID Transaksi</th>
					 <th>Tanggal</th>
					 <th>Nama Warga</th>
					 <th>Blok Rumah</th>
					 <th>RT</th>
					 <th>ID Iuran</th>
					 <th>Pembayaran</th>
					 <th>Total Harga</th>
					 <th>Keterangan</th>
					</tr>
					<?php
					$r = $con->query("SELECT tb_pembayaran.id_transaksi as id, tb_pembayaran.tanggal as tanggal, tb_warga.nama_warga as nama, tb_warga.blok_rumah as rumah, tb_warga.rt as rt, tb_pembayaran.id_iuran as iuran, tb_pembayaran.pembayaran as pemb, tb_pembayaran.total_harga as harga, tb_pembayaran.keterangan as keterangan FROM tb_pembayaran INNER JOIN tb_warga ON tb_pembayaran.no_kk = tb_warga.no_kk");
					while ($rr = $r->fetch_array()){
						?>
						<tr>
						<td><?php echo $rr['id'];?></td>
						<td><?php echo $rr['tanggal'];?></td>
						<td><?php echo $rr['nama'];?></td>
						<td><?php echo $rr['rumah'];?></td>
						<td><?php echo $rr['rt'];?></td>
						<td><?php echo $rr['iuran'];?></td>
						<td><?php echo $rr['pemb'];?></td>
						<td><?php echo $rr['harga'];?></td>
						<td><?php echo $rr['keterangan'];?></td>
						</tr>
						<?php
					}
				?>	
					</table>
                    </div>
					</div>
                </div>
            </div><!-- /.panel-->
</div>