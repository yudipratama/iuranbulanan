<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Users</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Master User</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading"><a href="index.php?page=saveuser" class="btn btn-primary">Tambah</a></div>
	<div class="panel-body">
		<div class="col-md-12">
			<div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>Nama Lengkap</th>
                        <th>Username</th>
                        <th>Authorization</th>
                        <th>Actions</th>
                    </tr>
                    <?php
                    $r = $con->query("SELECT * FROM user");
                    while ($rr = $r->fetch_array()) {
                        ?>
                        <tr>
                            <td><?php echo $rr['namalengkap'];?></td>
                            <td><?php echo $rr['username'];?></td>
                            <td><?php echo $rr['authorization'];?></td>
                            <td><a href="index.php?page=edituser&username=<?php echo $rr['username'];?>" class="btn btn-sm btn-success">Edit</a> <a href="controler/act_deluser.php?username=<?php echo $rr['username'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin hapus data ?')"> Hapus</a></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
		</div>
	</div>
</div>