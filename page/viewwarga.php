<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
                <li class="active">Warga</li>
			</ol>
		</div><!--/.row-->
        <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Warga</h1>
			</div>
		</div><!--/.row-->
        
    <div class="panel panel-default">
                <div class="panel-heading"><a href="index.php?page=savewarga" class="btn btn-primary">Tambah</a></div>
                <div class="panel-body">
                    <div class="col-md-12">
					<div class="table-responsive">
					
					<table class="table table-striped table-condensen">
					<tr>
					<th>NO KK</th>
					<th>NAMA WARGA</th>
					<th>JUMLAH KELUARGA</th>
					<th>NO HP</th>
					<th>BLOK RUMAH</th>
					<th>RT</th>
					<th>STATUS</th>
					</tr>
					<?php
					$r = $con->query("SELECT * FROM tb_warga");
					while ($rr = $r->fetch_array()){
						?>
						<tr>
						<tr>
						<td><?php echo $rr['no_kk'];?></td>
						<td><?php echo $rr['nama_warga'];?></td>
						<td><?php echo $rr['jumlah_keluarga'];?></td>
						<td><?php echo $rr['no_hp'];?></td>
						<td><?php echo $rr['blok_rumah'];?></td>
						<td><?php echo $rr['rt'];?></td>
						<td><?php echo $rr['status_pam'];?></td>
						<td><a class="btn btn-sm btn-primary" href="index.php?page=editwarga&no_kk=<?php echo $rr['no_kk'];?>">Edit</a> <a class="btn btn-sm btn-danger" href="controler/act_delwarga.php?no_kk=<?php echo $rr['no_kk'];?>" onclick="return confirm('Yakin ingin hapus data')">Hapus</a></td>
						</tr>
						<?php
					}
				?>	
					</table>
                    </div>
					</div>
                </div>
            </div><!-- /.panel-->
</div>